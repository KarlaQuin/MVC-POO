//Modelo que maneja la base datos

let notes = [
    {
        id: 1,
        title: 'Nota 1',
        description: 'Descripción de la nota 1'
    }
];  //Arreglo de objetos para almacenar los ids, titulos y descripciones de las notas

//Calse para simular la base de datos para ver, insertar, borrar y editar
class Notes{
    getNotes() {
        return notes;
    }

    insertNote(title, description) {
        let _notes = this.getNotes();
            console.log(_notes);
        let newNote = {
            id: _notes.length + 1,
            title: title,
            description: description,
        };

        notes.push(newNote);
    }

    deleteNote(id) {
        let _notes = this.getNotes();
        let index = _notes.findIndex(note => note.id == id);
            console.log("Index: "+index);
        if(index !== -1) {

            notes.splice(index, 1);
        }

        return notes;
    }

    editNote(id, newTitle, newDescription) {
        let _notes = this.getNotes();
        let note = _notes.find(note => note.id == id);

        if (note) {
            note.title = newTitle;
            note.description = newDescription;
        }

        return notes;
    }
}

exports.Notes = Notes;

