//Controlador de rutas
const fs = require('fs');
const path = require('path');
const view = path.join(__dirname, '..', 'view');
const ejs = require('ejs');
const querystring = require('querystring');

//importando modelos
const {Notes} = require('../models/notes');
const notes = new Notes();

//Clase Router que contiene métodos para cargar las páginas
class Router {
    //Página principal
    index(req, res){        
        const note = notes.getNotes();
        fs.readFile(path.join(view ,'index.html'), 'utf-8', (err, html) => {
            res.writeHead(200, { "Content-Type": 'text/html' });
            const renderedHTML = ejs.render(html, { data:note });
            res.end(renderedHTML);
        });
    }
    
    newNote(req, res){
        let body = '';
        req.on('data', (chunk) => {
            body += chunk;
        })

        req.on('end', () => {
            const formData = querystring.parse(body)
            console.log(formData);
            const {title, description} = formData;
            
            notes.insertNote(title, description);
            const note = notes.getNotes();        
            res.writeHead(302, { 'Location': '/' });
            res.end()
            
        });
    }

    editNote(id, req, res){        
        let body = '';
        req.on('data', (chunk) => {
            body += chunk;
        })

        req.on('end', () => {
            const formData = querystring.parse(body)
            console.log(formData);
            const {title, description} = formData;

            notes.editNote(id,title, description);
            res.writeHead(302, { 'Location': '/' });
            res.end()
        });
    }

    deleteNote(id, req, res){
        console.log(id)
        notes.deleteNote(id);
        res.writeHead(302, { 'Location': '/' });
        res.end()
    }
}

//Exportando la clase Router para que pueda ser utilizada en la aplicación (app.js)
exports.Router = Router;